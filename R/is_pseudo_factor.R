#' @title Check if vector is a one-hot pseudo-factor.
#' @description Check whether a vector is numeric and contains only 0, 1 and NA.
#' @details Oftentimes data will contain vectors that are de-facto factors
#' coding TRUE/FALSE for a given property. However, some functions require
#' inputs to explicitly be factors. Especially when using with dplyr::mutate_if
#' this function comes in handy for quickly transforming
#'
#' @param x A vector to evaluate.
#' @return Either TRUE if x is a _pseudoFactor_ or FALSE if it is not. A warning
#' is printed if x is a factor as as.numeric() on a factor leads to unexpected
#' results.
#' @examples
#' a <- c(0, 1, 0, 1)
#' is_pseudo_factor(a)
#' b <- c(2, 1, 5, 2)
#' is_pseudo_factor(b)
#' @export
is_pseudo_factor <- function(x) {
  if (is.factor(x)) {
    warning("Input is a factor.")
    return(FALSE)
  } else if (all(as.numeric(x) %in% c(0, 1, NA)) &&
             !all(is.na(as.numeric(x)))) {
    return(TRUE)
  } else {
    return(FALSE)
  }
}
