#' @title Data frame of counts
#' @description Get a data frame output of counts in a single step.
#' @details Transforming the output of table() to a data frame yields a
#' conveinient reperesntation of the data as a data frame. This function is
#' a wrapper around it -- aiming at interactive use -- that directly outputs
#' the data frame of counts.
#'
#' If working with data frames you need to specify the columns you are
#' interested in seperately.
#'
#' @param x The variable to get counts from.
#' @param ... Additional variables. As with table(), group combinations are
#' counted when supplying more than one variable.
#' @param keep_zero Bool whether to include 0 values in output.
#' @return A data frame of counts.
#' @examples
#' x <- sample(letters, 100, replace = TRUE)
#' table_df(x)
#' @export
table_df <- function(x, ..., keep_zero = TRUE) {
    data <- list(x, ...)
    tab <- table(data)
    tabd <- as.data.frame(tab)

    if (!keep_zero) {
        tabd <- tabd[tabd$Freq != 0, ]
    }

    return(tabd)
}
