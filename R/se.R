#' @title Standard Error of The Mean
#' @description Calculate the standard error of the mean on a vector.
#'
#' @param x The data to calculate the standard error from.
#' @param na.rm Whether to delete missing values.
#' @return The standard error of the mean.
#' @examples
#' x <- rnorm(50)
#' se(x)
#' @export
se <- function(x, na.rm = FALSE) {
    s <- stats::sd(x, na.rm = na.rm)
    len <- length(x)
    if (na.rm) {
        nas <- sum(is.na(x))
        len <- len - nas
    }
    se <- s / len
    return(se)
}
